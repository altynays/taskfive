package com.company;

public class Main
{
    public static void main( String[] args )
    {
        Pizza pizzaMargarita = new Pizza("Margarita", true, 2);
        pizzaMargarita.addIngredient(Pizza.Ingredient.GARLIC);
        pizzaMargarita.addIngredient(Pizza.Ingredient.BACON);
        pizzaMargarita.addIngredient(Pizza.Ingredient.TOMATO_PASTE);
        pizzaMargarita.addIngredient(Pizza.Ingredient.OLIVES);
        Pizza pizzaPepperoni = new Pizza("Pepperoni", false, 3);
        pizzaPepperoni.addIngredient(Pizza.Ingredient.PEPPERONI);
        pizzaPepperoni.addIngredient(Pizza.Ingredient.CHEESE);
        pizzaPepperoni.addIngredient(Pizza.Ingredient.CORN);
        pizzaPepperoni.addIngredient(Pizza.Ingredient.SALAMI);
        PizzaOrder firstOrder = new PizzaOrder(7717);
        firstOrder.addPizza(pizzaMargarita);
        firstOrder.addPizza(pizzaPepperoni);
        System.out.print(firstOrder + "\n");
        Pizza pizzaZZ = new Pizza("Base ZZ", false, 12);
        PizzaOrder secondOrder = new PizzaOrder(4372);
        secondOrder.addPizza(pizzaZZ);
        System.out.println(secondOrder);
    }
}

package com.company;

import java.util.ArrayList;

public class Pizza {
    private final static int MAX_NUMBER_OF_INGREDIENTS = 7;
    private String pizzaName;
    private int pizzaCount;
    private ArrayList<Ingredient> pizzaIngredients;

    public Pizza (String pizzaName, boolean isCalzone, int pizzaCount){
        pizzaIngredients = new ArrayList<>();
        this.pizzaName = pizzaName;
        this.pizzaCount = pizzaCount;
        if (isCalzone) {
            pizzaIngredients.add(Ingredient.PIZZA_TYPE_CALZONE);
        }
        else {
            pizzaIngredients.add(Ingredient.PIZZA_TYPE_ORDINARY);
        }
    }

    protected void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    protected void setPizzaCount(int pizzaCount) {
        this.pizzaCount = pizzaCount;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public int getPizzaCount() {
        return pizzaCount;
    }

    public double calculatePizzaCost(){
        if (pizzaIngredients == null) {
            return 0;
        } else {
            double cost = 0;
            for (Ingredient ingredient: pizzaIngredients){
                cost += ingredient.getPrice();
            }
            return cost;
        }
    }

    public void addIngredient(Ingredient newIngredient){
        if (pizzaIngredients.size() > MAX_NUMBER_OF_INGREDIENTS){
            System.out.println("You cannot add more than 7 ingredients!");
        } else {
            for (Ingredient ingredient : pizzaIngredients){
                if (ingredient.getName().equals(newIngredient.getName())){
                    System.out.println("There is already " + newIngredient.getName() + "in pizza. Try again.");
                    return;
                }
            }
            pizzaIngredients.add(newIngredient);
        }
    }

    public String toString(){
        String borderline = String.format("%32s", " ").replace(' ', '-') + "\n";
        StringBuilder stringToReturn = new StringBuilder("Name: ");
        stringToReturn.append(pizzaName)
                .append("\n")
                .append(borderline);
        for (Ingredient ingredient: pizzaIngredients){
            stringToReturn.append(String.format("%-27s",ingredient.getName()))
                    .append(String.format("%.2f",ingredient.getPrice()))
                    .append(" €\n");
        }
        return stringToReturn.append(borderline)
                .append("Total:")
                .append(String.format("%25.2f", calculatePizzaCost()))
                .append(" €\n")
                .append("Count:")
                .append(String.format("%27d", pizzaCount))
                .append("\n")
                .append(borderline)
                .toString();
    }

    public enum Ingredient {
        TOMATO_PASTE("Tomato Paste", 1.0),
        CHEESE("Cheese", 1.0),
        SALAMI("Salami", 1.5),
        BACON("Bacon", 1.2),
        GARLIC("Garlic", 0.3),
        CORN("Corn", 0.7),
        PEPPERONI("Pepperoni", 0.6),
        OLIVES("Olives", 0.5),
        PIZZA_TYPE_ORDINARY("Pizza Base (Ordinary)", 1.0),
        PIZZA_TYPE_CALZONE("Pizza Base (Calzone)", 1.5);

        private double price;
        private String name;

        Ingredient(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

    }

}

package com.company;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class PizzaOrder {
    public static final int MAX_NUMBER_OF_PIZZA = 10;
    private static int orderCount = 10000;
    private final int orderID;
    private final int clientID;
    private ArrayList<Pizza> pizzaArray;
    private LocalTime localTime;
    private DateTimeFormatter timeFormatted = DateTimeFormatter.ofPattern("HH:mm:ss");
    private PizzaNameCheck pizzaNameCheck = new PizzaNameCheck();

    public PizzaOrder (int clientId) {
        pizzaArray = new ArrayList<>();
        orderID = ++orderCount;
        this.clientID = clientId;
        localTime = LocalTime.now();
    }

    public int getPizzaOrder() {
        if (pizzaArray == null) {
            return 0;
        } else {
            int pizzaCount = 0;
            for (Pizza pizza : pizzaArray) {
                pizzaCount += pizza.getPizzaCount();
            }
            return pizzaCount;
        }
    }

    private void printPizzaAttributes(Pizza pizza) {
        String stringToPrint = "[" + orderID + " : " + clientID + " : " + pizza.getPizzaName() + " : " + pizza.getPizzaCount() + "]";
        System.out.println(stringToPrint);
    }

    public boolean addPizza(Pizza pizza) {
        if (getPizzaOrder() + pizza.getPizzaCount() > MAX_NUMBER_OF_PIZZA) {
            System.out.println("You cannot order more than 10 pizza!");
            return false;
        } else {
            if (!pizzaNameCheck.isValidPizzaName(pizza.getPizzaName())) {
                pizza.setPizzaName(clientID + "_" + (pizzaArray.size() + 1));
            }
            pizzaArray.add(pizza);
            printPizzaAttributes(pizza);
            return true;
        }
    }

    public class PizzaNameCheck {
        private final static int MIN_PIZZA_NAME_LENGTH = 4;
        private final static int MAX_PIZZA_NAME_LENGTH = 20;

        public boolean isValidPizzaName(String pizzaName) {
            return ((pizzaName.length() >= MIN_PIZZA_NAME_LENGTH) && (pizzaName.length() <= MAX_PIZZA_NAME_LENGTH));
        }
    }

    public boolean editPizzaCount(String pizzaName, int newCount) {
        for (Pizza pizza : pizzaArray) {
            if (pizza.getPizzaName().equals(pizzaName)) {
                if (getPizzaOrder() - pizza.getPizzaCount() + newCount > MAX_NUMBER_OF_PIZZA) {
                    System.out.println("You cannot order more than 10 pizza!");
                    return false;
                } else if (newCount == 0) {
                    return (pizzaArray.remove(pizza));
                } else {
                        pizza.setPizzaCount(newCount);
                        printPizzaAttributes(pizza);
                        return true;
                }
            }
        }
        return false;
    }

    public double calculateTotalCost() {
        if (pizzaArray == null) {
            return 0;
        } else {
            double totalCost = 0;
            for (Pizza pizza : pizzaArray) {
                totalCost += pizza.calculatePizzaCost() * pizza.getPizzaCount();
            }
            return totalCost;
        }
    }


    public String toString() {
        String borderline = String.format("%32s", " ").replace(' ', '*') + "\n";
        StringBuilder stringToReturn = new StringBuilder("\n" + borderline);
        stringToReturn.append("Time: ")
                .append(localTime.format(timeFormatted))
                .append("\n")
                .append("Order: ")
                .append(orderID)
                .append("\n")
                .append("Client: ")
                .append(clientID)
                .append("\n");
        if (pizzaArray == null) {
            return stringToReturn.append("Empty order")
                    .append("\n")
                    .append(borderline)
                    .toString();
        } else {
            for (Pizza pizza : pizzaArray) {
                stringToReturn.append(pizza);
            }
            return stringToReturn.append("Total sum:")
                    .append(String.format("%16.2f", calculateTotalCost()))
                    .append(" €")
                    .append("\n")
                    .append(borderline)
                    .toString();
        }
    }

}

